def main():
    # vraag naar de servernaam
    servernaam = input("wat is de servernaam: ")
    ipadres = input ("wat is het ip adres: ")
    netmask = input ("wat is het netmask: ")
    servernaam_clean = servernaam.strip()
    ipadres_clean = ipadres.strip()
    netmask_clean = netmask.strip()
    serverinfo = servernaam_clean + ',' + ipadres_clean + ',' + netmask_clean
    print("-"*12)
    print(serverinfo)
    print("=" * 12)
    print("servernaam:",servernaam)
    print("ip-adres: ",ipadres)
    print("subnetmask: ", netmask)

main()
