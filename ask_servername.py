def main():
    # vraag naar de servernaam
    servernaam = input("wat is de servernaam: ")
    ipadres = input ("wat is het ip adres: ")
    netmask = input ("wat is het netmask: ")
    serverinfo = servernaam + ',' + ipadres + ',' + netmask
    print("-"*12)
    print(serverinfo)
    print("=" * 12)
    print("servernaam:",servernaam)
    print("ip-adres: ",ipadres)
    print("subnetmask: ", netmask)

main()