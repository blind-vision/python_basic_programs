import sys 


def load_confuration():
    print("Loading configuration")

def save_configuration():
    print("Saving confuration")

def do_exit():
    print("Exiting")
    sys.exit(0)

def what_to_do():
    command = input("What to do? [load, save, quit]")
    command = command.strip() # strip surrounding spaces
    command = command.lower() # make case insensitive
    #of je kunt het combineren: command = command.strip().lower()

    if command == "load":
        load_configuration()
    elif command == "save":
        save_configuration()
    elif command == "quit":
        do_exit()
    else:
        print("Unkown command.")

def main():
    what_to_do()

main()
