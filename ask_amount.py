def ask_amount():
    '''Asks the user for an amount.

    Prints an error if the amount is smaller that 0 and returns 0.
    Else it will return the amount of the user.'''

    hoeveelheid = int(input("Wat is de hoeveelheid? "))
    if hoeveelheid < 0:
        print("That is not a positive number!")
        return 0  # fixture
    else:
        return hoeveelheid


def main():
    amount = ask_amount()
    message = "The user wants " + str(amount)
    print(message)


main()